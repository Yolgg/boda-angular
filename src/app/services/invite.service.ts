import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/';
import { Invite } from '../models/invite';
import { Global } from './global';

@Injectable()
export class InviteService{
	public url:string;

	constructor(

		private _http: HttpClient 
		){
			this.url = Global.url;
	}


	testService(){

		return 'Probando servicio';
	}


	saveInvite(invite: Invite): Observable<any>{

		let params = JSON.stringify(invite);
		let headers = new HttpHeaders().set('Content-Type','application/json');

		return this._http.post(this.url+'save-invite',params, {headers:headers});
	}

}