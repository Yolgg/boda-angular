import { Component, OnInit } from '@angular/core';
import * as countdown from 'countdown';

interface Time {
    days: number,
	hours: number,
	minutes: number,
	seconds : number

}

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
	time : Time = null;
	timerId: number = null;
  constructor() { }

  ngOnInit(): void {

  	let date = new Date("2021-07-24");
  	this.timerId = countdown(date,(ts) =>{
  		this.time = ts;
  		
  	}, countdown.DAYS | countdown.HOURS | countdown.MINUTES | countdown.SECONDS)
  	
  }

  ngOnDestroy(){

  	if(this.timerId){
  		clearInterval(this.timerId);
  	}
  }

}
