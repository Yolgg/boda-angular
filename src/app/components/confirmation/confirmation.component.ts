import { Component, OnInit } from '@angular/core';
import { Invite} from '../../models/invite';
import { InviteService} from '../../services/invite.service';


@Component({
  selector: 'app-confirmation',
  templateUrl: './confirmation.component.html',
  styleUrls: ['./confirmation.component.css'],
  providers: [InviteService]
})
export class ConfirmationComponent implements OnInit {

	public invite: Invite;
  public status: string;

  constructor(

  	private _inviteService: InviteService


  	) { 

  		this.invite = new Invite('','','','','',true);



  }

  ngOnInit(): void {
  }


  onSubmit(form){

  	
    this._inviteService.saveInvite(this.invite).subscribe(
      response => {
        
        if(response.invites){
          
          this.status = 'success';
           form.reset();
        }else{
          this.status= 'failed';
          
        }  

      },

      error => {

        console.log(<any>error);
      }

      )
  }

}
