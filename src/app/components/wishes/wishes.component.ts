import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-wishes',
  templateUrl: './wishes.component.html',
  styleUrls: ['./wishes.component.css']
})
export class WishesComponent implements OnInit {

	numberOfLikesL : number = 0;
	numberOfLikesBilly : number = 0;
	numberOfLikesT : number = 0;
	numberOfLikesOutdoor : number = 0;
	numberOfLikesFoto : number = 0;
	numberOfLikesMaldivas : number = 0;

  constructor() { }

  ngOnInit(): void {

  }

  likeButtonClickLuna(){

  	this.numberOfLikesL++;
  }

   likeButtonClickBilly(){

  	this.numberOfLikesBilly++;
  }

  likeButtonClickTrimestral(){

  	this.numberOfLikesT++;
  }
  likeButtonClickOutdoor(){

  	this.numberOfLikesOutdoor++;
  }

  likeButtonClickFoto(){

  	this.numberOfLikesFoto++;
  }
   likeButtonClickMaldivas(){

  	this.numberOfLikesMaldivas++;
  }


}
