export class Invite {

	constructor(
		public _id: string,
		public firstname: string,
		public lastname: string,
		public email: string,
		public intolerances: string,
		public coming: boolean
		
		){}
}