import {ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './components/home/home.component';
import { StoryComponent } from './components/story/story.component';
import { InfoComponent } from './components/info/info.component';
import { WishesComponent } from './components/wishes/wishes.component';
import { LocationComponent } from './components/location/location.component';
import { ConfirmationComponent } from './components/confirmation/confirmation.component';

const appRoutes: Routes = [
	{path: '', component: HomeComponent},
	{path: 'inicio', component: HomeComponent},
	{path: 'nuestra-historia', component: StoryComponent},
	{path: 'te-interesa', component: InfoComponent},
	{path: 'nuestros-deseos', component:WishesComponent},
	{path: 'ubicacion', component: LocationComponent},
	{path: 'confirmanos-vienes', component: ConfirmationComponent},
	{path: '**', component: HomeComponent}
];

export const appRoutingProviders: any[] = [];
export const routing: ModuleWithProviders<any> = RouterModule.forRoot(appRoutes);